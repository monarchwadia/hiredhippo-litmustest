(function() {
  $(document).on('turbolinks:load', function() {
    $('.nextButton').on('click', next);
    $('.backButton').on('click', back);
  })

  function next() {
    var currentPage = getCurrentPage();
    var nextPage = currentPage.next();

    if (nextPage.length > 0) {
      currentPage.hide();
      nextPage.show();
    }
  }

  function back() {
    var currentPage = getCurrentPage();
    var prevPage = currentPage.prev();

    if (prevPage.length > 0) {
      currentPage.hide();
      prevPage.show();
    }
  }

  function getCurrentPage() {
    return $('.onboarding-pages .onboarding-page:visible');
  }
})();