class PagesController < ApplicationController
  def index
    if user_signed_in?
      redirect_to '/onboard-candidate'
    end
  end

  def check_invite_code
    if check_invite_code_params[:invite_code] == 'hippo'
      redirect_to '/users/sign_up'
    else
      redirect_back(fallback_location: root_path)
    end
  end

  private

  def check_invite_code_params
    params.permit(:invite_code)
  end
end
