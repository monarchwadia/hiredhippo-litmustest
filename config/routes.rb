Rails.application.routes.draw do
  devise_for :users, controllers: {registrations: 'registrations'}
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'pages#index'
  post 'check_invite_code', to: 'pages#check_invite_code'
  get 'onboard-candidate', to: 'candidates#onboarding'
end
